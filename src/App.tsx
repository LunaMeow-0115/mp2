import Header from './components/Else/Header';
import Footer from './components/Else/Footer';
import Home from './components/HomeSearch/Home';
import ArtworkPages from './components/HomeSearch/ArtworkPages';
import GalleryArtwork from './components/Gallery/GalleryArtwork';
// 用最新版本的，Routes = Switch, useNavigation + useLocation = useHistory
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import {useState} from 'react';
import { ArtworkType } from './components/Else/Types';
import axios from "axios";

function App() {

    // NavSearch
    // 使用 useState('') 创建一个状态变量时，useState 会返回一个数组，第一个元素是当前的状态值，第二个元素是一个更新该状态值的函数。
    // search 获取当前状态值，setSearch 是一个函数，用于更新 search 的值。
    // useState一开始设定为空，代表一开始输入框里只显示placeholder，没有别的内容。
    // NavSearch.tsx中的input定义了value={search}，定义了输入框中会显示search的值。
    // 当用户在输入框中输入时，onChange 事件处理器会被触发，setSearch 函数会更新 search 的值。
    // e.target.value 表示输入框当前的search/value的值，通过调用 setSearch 并传入这个新值，你更新了 search 的值，从而导致组件重新渲染。
    const [search, setSearch] = useState<string>('');
    // 根据search query对应差找出的结果存在searchResults中。
    const [searchResults, setSearchResults] = useState<ArtworkType[]>([]);
    // 没搜索出结果之前显示loading状态。
    const [isLoading, setIsLoading] = useState<boolean>(false);

    // 第一次抓取API数据，由于通过search query的方式是没办法找到image_url和description和year和place，先设置成空。
    const firstFetchSearchResults = async () => {
        try {
            const response = await axios.get(`https://api.artic.edu/api/v1/artworks/search?q=${search}&limit=50`);
            const firstFectchData = response.data.data.map((item: any): ArtworkType => ({
                // item.id永远会有，只是可能api_link访问不了，但是这里已经设定成如果有问题就访问3260号的api_link。
                id: item.id,
                title: item.title ? item.title : 'Not Found',
                api_link: item.api_link ? item.api_link : 'https://api.artic.edu/api/v1/artworks/3260',
                image_url: '',
                place: '',
                year: '',
                description: ''
            }));
            return firstFectchData;
        } catch (error) {
            console.error("Error fetching search results:", error);
            return [];
        }
    };

    // 第二次抓取API数据，对第一次一次抓取的结果中的api_link再次抓取image_id和description。
    const secondFetchSearchResults = async (artwork: ArtworkType) => {
        try {
            const res = await axios.get(artwork.api_link);
            const data = res.data.data;
            // 设置 artwork.body。
            artwork.description = data.description ? data.description : 'Not Found';
            // 设置year。
            artwork.year = data.date_start ? data.date_start.toString() : 'Not Found';
            // 设置出产地。
            artwork.place = data.place_of_origin ? data.place_of_origin : 'Not Found';
            // 如果data.image_id存在，那么我们需要验证图像是否可以访问。
            if (data.image_id) {
                const potentialImageUrl = `https://www.artic.edu/iiif/2/${data.image_id}/full/843,/0/default.jpg`;
                // 验证图像的可访问性
                try {
                    await axios.head(potentialImageUrl);
                    artwork.image_url = potentialImageUrl;
                } catch (headError: any) {
                    // 如果访问不了image url，就用别的图代替。
                    if (headError.response && headError.response.status === 403) {
                        console.warn(`Image for artwork ${artwork.id} is not accessible.`);
                        artwork.image_url = `${process.env.PUBLIC_URL}/ImageNotFound.png`;
                    } else {
                        // 如果错误不是403，也设置成替代图片。
                        artwork.image_url = `${process.env.PUBLIC_URL}/ImageNotFound.png`;
                    }
                }
                // 如果image.id不存在，也设置成替代图片。
            } else {
                artwork.image_url = `${process.env.PUBLIC_URL}/ImageNotFound.png`;
            }
        } catch (err) {
            // 还有错误，也都设置成替代图片。
            console.error(`Error fetching details for artwork ${artwork.id}:`, err);
            artwork.image_url = `${process.env.PUBLIC_URL}/ImageNotFound.png`;
        }
    };

    // 当点击search按键，开始执行以下操作。
    const handleSearch = async (sortField: "year" | "title" | "none", sortOrder: "ascending" | "descending" | "none") => {
        // 开始加载。
        setIsLoading(true);
        // 第一次call API。
        const firstResults = await firstFetchSearchResults();
        // 第一次当中的每个结果都第二次call API。
        await Promise.all(firstResults.map(secondFetchSearchResults));

        // 选None的话就是这个自然结果
        let sortedResults = firstResults;

        // 如果没选None
        if (sortField !== "none" && sortOrder !== "none") {

            // 对第一次的结果进行排序
            sortedResults = firstResults.sort((a: any, b: any) => {

                // 按照year排序，注意year可能是number也可能是not found
                if (sortField === "year") {
                    let yearA = a.year === "Not Found" ? -Infinity : parseInt(a.year, 10);
                    let yearB = b.year === "Not Found" ? -Infinity : parseInt(b.year, 10);
                    return sortOrder === "ascending" ? yearA - yearB : yearB - yearA;

                    // 要么就是对title的字母顺序排序
                } else {
                    if (sortOrder === "ascending") {
                        return a.title.localeCompare(b.title);
                    } else {
                        return b.title.localeCompare(a.title);
                    }
                }
            });
        }

        // 设置排序后的结果
        setSearchResults(sortedResults);
        console.log(sortedResults);
        // 结束加载。
        setIsLoading(false);
    };

    return (
        <Router basename={'/mp2'}>
            <div className="App">

                {/* Header和Nav每个界面都要有不能变化。*/}
                <Header title="Art Institute of Chicago" />

                {/* 设定页面导向。*/}
                <Routes>

                    {/* home接受的是searchResults，所以显示的是过滤后的结果。*/}
                    {/* 初始化的界面上会显示Home->Map->一串结果的卡片信息。*/}
                    <Route path="/" element={<Home HomeInput={searchResults} Home_searchInput={search} Home_setSearchInput={setSearch} Home_handleSearchInput={handleSearch} Home_isLoadingInput={isLoading} />} />

                    {/* 点击卡片的title和datetime和图片区域将会导航到artwork的具体信息界面。*/}
                    <Route path="/artwork/:id" element={<ArtworkPages ArtworkPagesInput={searchResults}/>} />

                    {/* /gallery之后还要分层级，所以这样写。 */}
                    <Route path="/gallery/*" element={<GalleryArtwork />} />

                </Routes>

                {/* Footer每个界面都有不能改变。*/}
                <Footer />

            </div>
        </Router>
    );
}

export default App;


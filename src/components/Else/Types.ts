
export type ArtworkType = {
    id: number;
    title: string;
    place: string;
    description: string;
    api_link: string;
    year: string;
    image_url: string;
};
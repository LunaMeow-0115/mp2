import React from 'react';
import {Link} from "react-router-dom";

// tsx的特殊写法
interface HeaderProps {
    title: string;
}

const Header: React.FC<HeaderProps> = ({ title }) => {
    return (
        <header className="Header">
            <h1>{title}</h1>

            {/* 导航按钮 */}
            <ul>
                <li><Link to={"/"}>Home</Link></li>
                <li><Link to={"/gallery"}>Gallery</Link></li>
            </ul>

        </header>

    );
}

export default Header;


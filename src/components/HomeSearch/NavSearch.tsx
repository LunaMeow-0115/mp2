import React, { useState } from 'react';

interface Props {
    NavSearch_QueryInput: string;
    NavSearch_setSearch: (value: string) => void;
    // 更新NavSearch_clickSearch的参数类型
    NavSearch_clickSearch: (sortField: "none" | "year" | "title", sortOrder: "none" | "ascending" | "descending") => void;
    Nav_Search_isLoading: boolean;
}

const NavSearch: React.FC<Props> = ({ NavSearch_QueryInput, NavSearch_setSearch, NavSearch_clickSearch, Nav_Search_isLoading }) => {

    // 新增sortField和sortOrder的状态
    const [sortField, setSortField] = useState<"year" | "title" | "none">("none");
    const [sortOrder, setSortOrder] = useState<"ascending" | "descending" | "none">("none");

    return (
        <nav className="Nav">

            <form className="searchForm" onSubmit={(e) => {
                e.preventDefault();
                // 在搜索时传递sortField和sortOrder参数
                NavSearch_clickSearch(sortField, sortOrder);
            }}>

                <label htmlFor="search"> Search Artwork:</label>

                <input
                    id="search"
                    type="text"
                    placeholder="Enter..."
                    value={NavSearch_QueryInput}
                    onChange={(e) => NavSearch_setSearch(e.target.value)}
                />

                {/* 下拉菜单允许用户选择排序字段 */}
                <label>Sort by:</label>
                    <select value={sortField} onChange={(e) => setSortField(e.target.value as "year" | "title" | "none")}>
                        <option value="none">None</option>
                        <option value="title">Title</option>
                        <option value="year">Year</option>
                    </select>

                {/* 单选按钮允许用户选择排序方式 */}
                <div>

                    <label>
                        <input
                            type="radio"
                            value="ascending"
                            checked={sortOrder === "ascending"}
                            onChange={() => setSortOrder("ascending")}
                        />
                        Ascending
                    </label>

                    <label>
                        <input
                            type="radio"
                            value="descending"
                            checked={sortOrder === "descending"}
                            onChange={() => setSortOrder("descending")}
                        />
                        Descending
                    </label>

                </div>
                <button type="submit">Search</button>

            </form>

            {/* loading的时候显示的东西 */}
            {Nav_Search_isLoading && <img src={`${process.env.PUBLIC_URL}/Spinner.svg`} alt="Loading..." className="loading-image" />}

        </nav>
    );
}

export default NavSearch;


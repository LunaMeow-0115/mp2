import React from 'react';
import { ArtworkType } from '../Else/Types'; // assuming types.ts is in the same directory
import SearchResultsCard from './SearchResultsCard';

interface Props {
    MapSearchResultsInput: ArtworkType[];
}

// 把收到的searchResults list中的每一个项，都转化为SearchResultsCard component：
// key等于每个artwork的id，value即为整个artwork的全部内容。
const MapSearchResults: React.FC<Props> = ({ MapSearchResultsInput }) => {
    return (
        <>
            {MapSearchResultsInput.map(artwork => (
                <SearchResultsCard
                    key={artwork.id}
                    SearchResultsCardInput={artwork}/>
            ))}
        </>
    );
}

export default MapSearchResults;
import { ArtworkType } from '../Else/Types';
import MapSearchResults from './MapSearchResults';
import React from "react";
import NavSearch from "./NavSearch";

interface Props {
    HomeInput: ArtworkType[];
    Home_searchInput: string;
    Home_setSearchInput: (value: string) => void;
    Home_handleSearchInput: (sortField: "year" | "title" | "none", sortOrder: "ascending" | "descending" | "none") => void;
    Home_isLoadingInput: boolean;
}

const Home: React.FC<Props> = ({ HomeInput, Home_searchInput, Home_setSearchInput, Home_handleSearchInput, Home_isLoadingInput}) => {
    return (
        <div className="Home">

            {/* 实例化Nav组件，将search，setSearch，SearchFun_nav，isLoading变量作为一个props传给Nav.tsx，相当于call function了。
               尾缀为com证明对应的变量是在NavSearch.tsx中的，没有尾缀证明是在App.tsx中。*/}
            <NavSearch NavSearch_QueryInput={Home_searchInput}
                       NavSearch_setSearch={Home_setSearchInput}
                       NavSearch_clickSearch={Home_handleSearchInput}
                       Nav_Search_isLoading={Home_isLoadingInput} />


            {/* 如果searchResults是有长度的，也就是说搜索结果不为空 */}
            {HomeInput.length ? (
                // 那就给searchResults这个list传到Feed中
                <MapSearchResults MapSearchResultsInput={HomeInput} />
            ) : (
                // 如果list为空，就显示没有result
                <p className="center-text">
                    No result to display for now.
                    You can try to:
                    1. Wait while the content is loading.
                    2. There is no result for this search, try to do another search.
                </p>
            )}

        </div>
    )
}

export default Home;


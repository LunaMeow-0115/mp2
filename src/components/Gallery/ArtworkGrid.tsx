import React from 'react';
import GalleryResultsCard from "./GalleryResultsCard"; // 请确保路径是正确的

type Props = {
    ArtworkGridInput: any[];
};

const ArtworkGrid: React.FC<Props> = ({ ArtworkGridInput }) => {

    console.log("Grid", ArtworkGridInput);
    return (
        <div className="artwork-grid">
            {ArtworkGridInput.map(artwork => (
                <GalleryResultsCard
                    key={artwork.id}
                    GalleryResultsCardInput={artwork} />
            ))}
        </div>
    );
}

export default ArtworkGrid;


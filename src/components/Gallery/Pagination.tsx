/*
type Props = {
    currentPage: number;
    totalPages: number;
    onPageChange: (page: number) => void;
};

const Pagination: React.FC<Props> = ({ currentPage, totalPages, onPageChange }) => {
    return (
        <div className="Pagination">
            <button disabled={currentPage <= 1} onClick={() => onPageChange(currentPage - 1)}>Previous</button>
            <span>{currentPage} / {totalPages}</span>
            <button disabled={currentPage >= totalPages} onClick={() => onPageChange(currentPage + 1)}>Next</button>
        </div>
    );
}

export default Pagination;
*/
type PaginationProps = {
    currentPage: number;
    totalPages: number;
    onPageChange: (page: number) => void;
};

const Pagination: React.FC<PaginationProps> = ({ currentPage, totalPages, onPageChange }) => {
    const pages = Array.from({ length: totalPages }, (_, i) => i + 1); // [1, 2, 3, ...]

    return (
        <div className="Pagination">
            <label>Chose Page Number: </label>
            <select value={currentPage} onChange={e => onPageChange(Number(e.target.value))}>
                {pages.map(page => (
                    <option key={page} value={page}>
                        {page}
                    </option>
                ))}
            </select>
        </div>
    );
}
export default Pagination;
import React, {useEffect, useState} from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
import { ArtworkType } from '../Else/Types';

interface Props {
    GalleryDetailsInput: ArtworkType[];
}

// 展现artwork细节的详细信息，并且是轮播的形式，显示当前artwork的信息，也可以查前后的信息，因为这个component的input是一整个list of artwork。
const GalleryDetails: React.FC<Props> = ({ GalleryDetailsInput }) => {
    console.log("GalleryDetailsInput", GalleryDetailsInput);

    // 负责变换url的id
    const navigate = useNavigate();

    // 钩子钩出来进入ArtworkPage的url的id。
    const { id } = useParams<{ id: string }>();
    console.log("钩子:", id);

    // 咋到这个id对应的artwork，并且记录他在list of artwork里的index。
    const initialIndex = GalleryDetailsInput.findIndex(artwork => artwork.id.toString() === id);
    console.log("initialIndex:", initialIndex);

    // 一个状态记录展示的artwork的id。
    const [currentIndex, setCurrentIndex] = useState(initialIndex);

    //加入useEffect，只要id变了就可以进行改变，这样用户可以直接从url上改id来查看页面，不用通过点击卡片。
    useEffect(() => {
        const newIndex = GalleryDetailsInput.findIndex(artwork => artwork.id.toString() === id);
        setCurrentIndex(newIndex);
    }, [id, GalleryDetailsInput]);

    // 切换到前一个。
    const goToPrevArtwork = () => {
        if (currentIndex > 0) {
            setCurrentIndex(currentIndex - 1);
            const prevArtworkId: number = GalleryDetailsInput[currentIndex - 1].id;
            // 注意设定的是绝对路径
            navigate(`/gallery/gallery-details/${prevArtworkId}`);
        }
    };
    // 切换到后一个。
    const goToNextArtwork = () => {
        if (currentIndex < GalleryDetailsInput.length - 1) {
            setCurrentIndex(currentIndex + 1);
            const nextArtworkId: number = GalleryDetailsInput[currentIndex + 1].id;
            // 注意设定的是绝对路径
            navigate(`/gallery/gallery-details/${nextArtworkId}`);
        }
    };

    // 当前要展示出来的artwork。
    const artwork = GalleryDetailsInput[currentIndex];

    return (
        // 注意!!! 如果className和component同名！会导致css问题！所以这里我写的是GalleryDetail没有s！
        <div className="GalleryDetail">
            {/* 前后按钮 */}
            <button className="previous" onClick={goToPrevArtwork} disabled={currentIndex === 0}>Previous</button>
            <button className="next" onClick={goToNextArtwork} disabled={currentIndex === GalleryDetailsInput.length - 1}>Next</button>
            <div className="GalleryDetailsInfo">
                {artwork &&
                    <>
                        <h2 className="title">{artwork.title}</h2>
                        <p className="detailkyear"><strong>Year: </strong>{artwork.year}</p>
                        <p className="detailplace"><strong>Place of Origin: </strong> {artwork.place}</p>
                        <p className="detaildesc"> <strong>Description: </strong></p>
                        <div className="detailDesc" dangerouslySetInnerHTML={{ __html: artwork.description }}></div>
                        {/* 如果image url有问题，有替换图片。 */}
                        <img
                            src={artwork.image_url}
                            alt={artwork.title}
                            onError={(e) => { e.currentTarget.src = `${process.env.PUBLIC_URL}/ImageNotFound.png`; }}
                        />
                    </>
                }
                {!artwork &&
                    <>
                        <h2>Artwork Not Found</h2>
                        <p>Well, that's disappointing.</p>
                        <p>
                            <Link to='/'>Visit Our Homepage</Link>
                        </p>
                    </>
                }
            </div>
        </div>
    );
}

export default GalleryDetails;


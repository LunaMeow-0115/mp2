import React, {useState, useEffect} from 'react';
import CategorySelect from './CategorySelect';
import Pagination from './Pagination';
import GalleryDetails from "./GalleryDetails";
import ArtworkGrid from './ArtworkGrid'; // 根据实际路径调整
import { ArtworkType } from '../Else/Types';
import axios from "axios";
import { Routes, Route } from 'react-router-dom';
import { useLocation } from 'react-router-dom';

const GalleryArtwork: React.FC = () => {
    // 记录初选择了哪一个category，初始值是All。
    const [selectedCategory, setSelectedCategory] = useState<string>('All');
    // 记录当前页吗。
    const [currentPage, setCurrentPage] = useState<number>(1);
    // 总页码设定为20页。
    const totalPages = 20;
    // 记录删选完的结果。
    const [artworksCateResults, setArtworksCateResults] = useState<ArtworkType[]>([]);
    // 判断是否在Loading。
    const [isLoading, setIsLoading] = useState<boolean>(false);
    // 判断当前的url是什么，来决定要不要显示category bar。
    const location = useLocation();

    useEffect(() => {
        const fetchData = async () => {

            setIsLoading(true);

            // 每个page抓取100个结果。
            const response = await axios.get(`https://api.artic.edu/api/v1/artworks?page=${currentPage}&limit=100`);

            // 先判断image_id和url的有效性。
            const checkImageUrl = async (image_id: string) => {
                const imageUrl = `https://www.artic.edu/iiif/2/${image_id}/full/843,/0/default.jpg`;
                try {
                    // 使用HEAD请求检查图片URL是否有效
                    await axios.head(imageUrl);
                    return imageUrl;
                } catch (error: any) {
                    if (error.response) {
                        // The request was made and the server responded with a status code
                        // that falls out of the range of 2xx
                        console.error(`Error fetching image: ${error.response.status} - ${error.response.statusText}`);
                    } else if (error.request) {
                        // The request was made but no response was received
                        console.error("No response received:", error.request);
                    } else {
                        // Something else happened in setting up the request that triggered an Error
                        console.error("Error", error.message);
                    }
                    return `${process.env.PUBLIC_URL}/ImageNotFound.png`;
                }
            };

            const dataPromises = response.data.data.map(async (item: any): Promise<ArtworkType> => {
                // 如果有image_id就检查image url，如果没有直接设定成Not Found图片，
                const imageUrl = item.image_id ? await checkImageUrl(item.image_id) : `${process.env.PUBLIC_URL}/ImageNotFound.png`;
                return {
                    // item.id永远会有，只是可能api_link访问不了，但是这里不需要api_link所以不管。
                    id: item.id,
                    title: item.title ? item.title : 'Not Found',
                    api_link: '',
                    image_url: imageUrl,
                    year: item.date_start ? item.date_start.toString() : 'Not Found',
                    description: item.description ? item.description : 'Not Found',
                    place: item.place_of_origin ? item.place_of_origin : 'Not Found'
                };
            });

            const data = await Promise.all(dataPromises);

            // Rome和Rome Empire算成一起。检查是否包含 "Rome"
            const filterForRome = (artwork: ArtworkType) =>
                artwork.place.includes("Rome");

            // 如果选择了category，不是All。
            if (selectedCategory !== 'All') {
                let filteredData;
                if (selectedCategory === 'Rome') {
                    filteredData = data.filter(filterForRome);
                } else {
                    // 其他类型就设置成其他selectedCategory。
                    filteredData = data.filter((artwork: ArtworkType) =>
                        artwork.place === selectedCategory);
                }
                console.log(filteredData)
                setArtworksCateResults(filteredData);
            } else {
                // All的话直接显示。
                console.log(data)
                setArtworksCateResults(data);
            }
            setIsLoading(false);  // 完成加载时设置为 false
        }

        fetchData();

        // 根据选择的类型和页码来变动。
        }, [selectedCategory, currentPage]);
    //}, [selectedCategory]);

    // 类型按钮选择。
    const handleCategorySelect = (category: string) => {
        setSelectedCategory(category);
        setCurrentPage(1);
    }


    // 页码按钮选择。
    const handlePageChange = (page: number) => {
        setCurrentPage(page);
    }


    console.log(location.pathname)

    return (
        <div className='Gallery'>

            {/* 只有在路径不是 /gallery/gallery-details/:id 时才显示组合区域 */}
            {location.pathname.startsWith("/gallery/gallery-details") ? null : (
                <div className='filters-container'>
                    <div className="category-select">
                        <CategorySelect onCategorySelect={handleCategorySelect} />
                    </div>
                    <br></br>
                    <div className="pagination-select">
                        <Pagination currentPage={currentPage} totalPages={totalPages} onPageChange={handlePageChange} />
                    </div>
                </div>
            )}

            <Routes>
                {/*定义子路径。注意写法。即/gallery/的子路径，gallery-details/:id*/}
                <Route path="gallery-details/:id" element={<GalleryDetails GalleryDetailsInput={artworksCateResults} />} />
                {/*如果路径是/gallery，就判断以下。在这里，/就等于/gallery了，因为GalleryArtwork界面已经是/gallery了。*/}
                <Route path="/" element={
                    <>
                        {isLoading ? (
                            <div className="center-image">
                                <img src={`${process.env.PUBLIC_URL}/Spinner.svg`} alt="Loading spinner" className="loading-image" />
                                {/*}
                                <img src={'Spinner.svg'} alt="Loading spinner" className="loading-image" />
                                   */}
                            </div>
                        ) : artworksCateResults.length === 0 ? (
                            <p className="center-text">This page does not have any record, please try next page or redo your search.</p>
                        ) : (
                            <ArtworkGrid ArtworkGridInput={artworksCateResults} />
                        )}
                    </>
                } />
            </Routes>

        </div>
    );
}

export default GalleryArtwork;

/* 计算前2000个artwork的records，计算每个的place of origin的counts。
interface ArtworkData {
    id: number;
    place_of_origin: string;
}
const [originCounts, setOriginCounts] = useState<Map<string, number>>(new Map());
useEffect(() => {
    const fetchArtworks = async () => {
        let originCounts: Map<string, number> = new Map();
        console.log("start");
        for (let page = 1; page <= 20; page++) {
            const response = await fetch(`https://api.artic.edu/api/v1/artworks?page=${page}&limit=100`);
            const data: { data: ArtworkData[] } = await response.json();
            data.data.forEach(item => {
                if (item.place_of_origin) {
                    const currentCount = originCounts.get(item.place_of_origin) || 0;
                    originCounts.set(item.place_of_origin, currentCount + 1);
                }
            });
        }
        setOriginCounts(originCounts);
        console.log(originCounts);
        console.log("end");
        setIsLoading(false);
    }

    fetchArtworks();
}, []);
if (isLoading) {
    return <p>Loading...</p>
}
 */
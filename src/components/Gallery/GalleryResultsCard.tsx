import React from 'react';
import {Link} from 'react-router-dom';
import { ArtworkType } from '../Else/Types';

interface Props {
    GalleryResultsCardInput: ArtworkType;
}

// 将searchResults的每一个结果都做成一个卡片显示出来。
// input是MapSearchResults中的artwork list当中其中一个artwork。
const GalleryResultsCard: React.FC<Props> = ({ GalleryResultsCardInput }) => {
    console.log(GalleryResultsCardInput.id)
    return (
        <div className="GalleryResultsCard">

            {/* 接下来的内容将成为一个可以点击的link，导向/artwork/artwork.id，根据App.tsx的定义，即为ArtworkPages界面。*/}
            <Link to={`gallery-details/${GalleryResultsCardInput.id}`}>

                <h3 className="title"> {GalleryResultsCardInput.title} </h3>
                <p className="year"> <strong>Year: </strong> {GalleryResultsCardInput.year} </p>
                <p className="place"> <strong>Place of Origin: </strong>{GalleryResultsCardInput.place} </p>

                {/* image_url能成功就显示图片，不能成功就显示默认图片。*/}
                <img
                    className="image"
                    src={GalleryResultsCardInput.image_url}
                    alt={GalleryResultsCardInput.title}
                    onError={(e) => { e.currentTarget.src = `${process.env.PUBLIC_URL}/ImageNotFound.png`; }}
                />

            </Link>

        </div>
    );
}

export default GalleryResultsCard;


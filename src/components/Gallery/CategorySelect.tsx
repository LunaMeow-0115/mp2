import React from "react";

type Props = {
    onCategorySelect: (category: string) => void;
};

const CategorySelect: React.FC<Props> = ({ onCategorySelect }) => {
    const categories = [
        'All', 'United States', 'France', 'England',
        'China', 'Netherlands', 'Japan', 'Germany',
        'Rome', 'Spain', 'Flanders', 'Syria',
        'Italy', 'Egypt', 'Belgium', 'Holland'
    ];

    return (
        <div className="category-select">
            <label> Choose a Category: </label>
            {/* 初始的default value是All，value会根据选择而变化*/}
            <select defaultValue="All" onChange={e => onCategorySelect(e.target.value)}>
                {/* 菜单里显示出categories */}
                {categories.map(category => (
                    <option key={category} value={category}>
                        {category}
                    </option>
                ))}
            </select>
        </div>
    );
}

export default CategorySelect;

